import cv2
import sys
import numpy as np
import os
import json
import time
from random import randrange

# Path to the directory containing all the images
if os.path.exists(sys.argv[1]):
    path = sys.argv[1]
    if path.endswith('/'):
        pass
    elif path.endswith('.jpg'):
        pass
    elif path.endswith('.png'):
        pass
    else:
        print('Add an "/" at the end of the path')
        sys.exit()
else:
    print('\nINCORRECT PATH\n')
    exit()


class JustCoordinates():
    def __init__(self, image_name, json_name):
        self.frame = cv2.imread(image_name)
        self.json_handler = JSONHandle(json_name)
        self.coordinates = []
        self.height, self.width = self.frame.shape[:2]

    def show_image(self):
        # print('\n' + f + '\n')
        print('Draw the ROI and then press space.')
        cv2.namedWindow('roi_window', cv2.WINDOW_NORMAL)
        cv2.setMouseCallback("roi_window", self.click_and_crop)
        cv2.imshow('roi_window', self.frame)
        
    def click_and_crop(self, event, x, y, flags, p):
        if event == cv2.EVENT_LBUTTONDOWN:
            return
        elif event == cv2.EVENT_LBUTTONUP:
            print("click-click-mufaka")

            x = x/self.width
            y = y/self.height
            self.coordinates.append([x,y])
            return
        elif event == cv2.EVENT_MOUSEMOVE:
            return

    def save_coordinates(self):  
        self.json_handler.append_json(self.coordinates)
        self.json_handler.dump_json_to_file()
        self.coordinates = []


class ImageHandler():
    def __init__(self, image_name, json_name):
        self.frame = cv2.imread(image_name)
        self.json_handler = JSONHandle(json_name)
        self.coordinates = []
        self.dup_coordinates = []
        self.height, self.width = self.frame.shape[:2]

    def show_image(self):
        # print('\n' + f + '\n')
        print('Draw the ROI and then press space.')
        cv2.namedWindow('roi_window', cv2.WINDOW_NORMAL)
        cv2.setMouseCallback("roi_window", self.click_and_crop)
        cv2.imshow('roi_window', self.frame)
        
    def click_and_crop(self, event, x, y, flags, p):
        if event == cv2.EVENT_LBUTTONDOWN:
            return
        elif event == cv2.EVENT_LBUTTONUP:
            print("click-click-mufaka")

            x = x/self.width
            y = y/self.height
            self.coordinates.append([x,y])
            return
        elif event == cv2.EVENT_MOUSEMOVE:
            return

    def save_coordinates(self, id, type, functionalities, order, lane_number):
        if type == 'BEST_SHOT':
            block = {"id": id,"type": type, "functionalities": functionalities, "coordinates": self.coordinates, "order": order}
        elif type == 'ORDER':
            block = {"id": id,"type": type, "functionalities": functionalities, "lane_number": int(lane_number),"coordinates": self.coordinates}
        elif type == 'WAITING':
            block = {"id": id,"type": type, "functionalities": functionalities,"coordinates": self.coordinates, "interaction_roi": True}
        elif type == 'LICENCE_PLATE_CAPTURE':
            block = {"id": id,"type": type, "functionalities": functionalities, "predict": True, "store": True,"coordinates": self.coordinates}
        else:
            block = {"id": id,"type": type, "functionalities": functionalities,"coordinates": self.coordinates}

        block = {"id": id,"type": type, "functionalities": functionalities,"coordinates": self.coordinates}
        
        if 'best_shot' in functionalities:
            block['order'] = order
        if 'lpr' in functionalities:
            block["predict"] = True
            block["store"] = True
        if 'ORDER' == type:
            block['lane_number'] = int(lane_number)
        elif 'WAITING' == type:
            block['interaction_roi'] = True
        
        
        self.json_handler.append_json(block)
        self.json_handler.dump_json_to_file()
        functionalities = []
        self.dup_coordinates = self.coordinates
        self.coordinates = []

    def duplicate_coordinates(self, id, type, functionalities, order, lane_number):
        if type == 'BEST_SHOT':
            block = {"id": id,"type": type, "functionalities": functionalities,"coordinates": self.dup_coordinates, "order": order}
        elif type == 'ORDER':
            block = {"id": id,"type": type, "functionalities": functionalities, "lane_number": int(lane_number),"coordinates": self.dup_coordinates}
        elif type == 'WAITING':
            block = {"id": id,"type": type, "functionalities": functionalities,"coordinates": self.dup_coordinates, "interaction_roi": True}
        elif type == 'LICENCE_PLATE_CAPTURE':
            block = {"id": id,"type": type, "functionalities": functionalities, "predict": True, "store": True,"coordinates": self.dup_coordinates}
        else:
            block = {"id": id,"type": type, "functionalities": functionalities,"coordinates": self.dup_coordinates}

        self.json_handler.append_json(block)
        self.json_handler.dump_json_to_file()

class JSONHandle():
    def __init__(self, file_name):
        self.file_name = file_name

        if os.path.exists(self.file_name):
            print(self.file_name + " does exist.")
            with open(self.file_name) as f:
                self.data = json.load(f)
        else:
            self.data = []

    def append_json(self, new_json):
        self.data.append(new_json)
    
    def dump_json_to_file(self):
        with open(self.file_name, 'w') as f:
            json.dump(self.data, f, indent=4)

def get_functionality():
    functionalities = []
    valid_functionalities = ['lpr', 'stream_roi', 'best_shot', 'timer']
    another_func = '1'
    while another_func == '1':
        print('\n\n--- LIST OF FUNCTIONALITIES ---')
        for func in valid_functionalities:
            print(func)
        print('---------------------------')
        func = input('\nWhich of the above functionalities do you want to add?\nPress 0 to skip\n')

        if func == '0':
            another_func = '0'
            pass
        else:
            if func in valid_functionalities:
                print('valid')
                functionalities.append(func)
            else:
                print('Functionality is not valid!')
                pass
    return functionalities

def get_id_and_type():
    # Get the current ROI's ID and TYPE
    block_entered_id = input('\nWhat is this ROIs ID?\n')

    if (block_entered_id.find('start') != -1):
        block_type = 'START'
        block_id = 'start_' + str(randrange(1000))
    elif (block_entered_id.find('order') != -1):
        block_type = 'ORDER'
        block_id = 'order_' + str(randrange(1000))
    elif (block_entered_id.find('payment') != -1):
        block_type = 'PAYMENT'
        block_id = 'payment_' + str(randrange(1000))
    elif (block_entered_id.find('deliver') != -1):
        block_type = 'DELIVER'
        block_id = 'deliver_' + str(randrange(1000))
    elif (block_entered_id.find('wait') != -1):
        block_type = 'WAITING_BAY'
        block_id = 'deliver_' + str(randrange(1000))
    elif (block_entered_id.find('pull') != -1):
        block_type = 'PULL_FORWARD'
        block_id = 'pull_forward' + str(randrange(1000))
    elif (block_entered_id.find('licence') != -1) or (block_entered_id.find('lpr') != -1):
        block_type = 'LICENCE_PLATE_CAPTURE'
        block_id = 'lpr_' + str(randrange(1000))
    elif (block_entered_id.find('best') != -1):
        block_type = 'BEST_SHOT'
        block_id = 'best_shot_' + str(randrange(1000))
    else:
        block_type = input('\nWhat is this ROIs TYPE?\n')

    return block_id, block_type

def get_order(block_type):
    order_confirm = '1'

    while order_confirm == '1':
        if block_type == 'BEST_SHOT':
            order = input('\nWhat is the order of this camera?\nPress 0 to skip\n')
            if order_confirm == '0':
                order = 'REMEMBER TO ADD ORDER'
        else:
            order = ''
        order_confirm = '2'

    return order

def get_lane_number(block_type):
    if block_type == 'ORDER':
        lane_number = input('\nEnter the lane number:\n')
    else:
        lane_number = ''
    
    return lane_number


def draw_block_or_coordinates():
    print(path + '\n')
    camera_number = input('Camera Number?\n')
    json_name = 'camera{}.json'.format(camera_number)
    block_or_coordinates = input('\n1 = JUST COORDINATES\n2 = ROI BLOCK\n')

    if block_or_coordinates == '1':
        # print(f)
       
        coordinates_image = JustCoordinates(path_to_file, json_name)
        coordinates_image.show_image()
        os.system("wmctrl -a roi_window")
        cv2.waitKey(0) & 0xFF == ord('q')
        print(json_name + '\n')
        coordinates_image.save_coordinates()
    elif block_or_coordinates == '2':
        # print(f)
        
        current_image = ImageHandler(path_to_file, json_name)
        current_image.show_image()

        second_block = '1'
        while second_block == '1':
            os.system("wmctrl -a roi_window")
            cv2.waitKey(0) & 0xFF == ord('q')
            print(json_name + '\n')

            block_id, block_type = get_id_and_type()
            # func = get_functionality()
            order = get_order(block_type)
            lane_number = get_lane_number(block_type)
            func = get_functionality()

            current_image.save_coordinates(block_id, block_type, func, order, lane_number)
            second_block = input('\nEnter 0 if you dont want another ROI\nEnter 1 if you want to add another ROI\nEnter 2 to duplicate the ROI\n')
            
            if second_block == '2':
                while second_block == '2':
                    block_id, block_type = get_id_and_type()
                    order = get_order(block_type)
                    lane_number = get_lane_number(block_type)

                    current_image.duplicate_coordinates(block_id, block_type, func, order, lane_number)

                    print('\nduplicated the last block\n')
                    second_block = input('\nEnter 0 if you dont want another ROI\nEnter 1 if you want to add another ROI\nEnter 2 to duplicate the ROI\n')
            else:
                pass


# Just for one image
if path.endswith('.jpg') or path.endswith('.png'):
    path_to_file = path
    
    draw_block_or_coordinates()

# For a directory containing images
else:
    files = os.listdir(path)
    for f in files:
        path_to_file = path + str(f)
        print(f)
        draw_block_or_coordinates()

cv2.destroyAllWindows()