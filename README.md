# roi_tool

### For multiple images that needs ROIs, put all in one directory, and add the path of the directory as an argument when running the script. Example, `python3 roi.py /home/jacques/images_folder/` (Include the last '/'). And then follow the instructions.

### For a single image, do the same as above just include the path to the image. Example, `python3 roi.py /home/jacques/images_folder/image.jpg`

### A json file will be created with thr name of the camera number